[Boosted electric skateboards](https://sharkwheel.com/electric-longboard/
) are a form of battery-operated electric skateboard. These skateboards can reach speeds of over 20 miles per hour and can be used anywhere a regular skateboard can be used. 
These boards provide consumers with a quick and efficient way to navigate about.
<img src="https://cdn11.bigcommerce.com/s-oq4cmf0wjd/stencil/c8b69c00-d997-0139-38e0-56a23bbfb194/e/f691c990-5a7b-013a-ffe7-42126914a9ac/img/sharkwheel-logo.svg">
